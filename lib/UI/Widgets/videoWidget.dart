import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:video_player/video_player.dart';

class VideoWidget extends StatefulWidget {

  final String url;


  VideoWidget(this.url);

  @override
  State<VideoWidget> createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {

  late VideoPlayerController _videoPlayerController;
  ChewieController? _chewieController;

  Future<void> init() async{
    _videoPlayerController = VideoPlayerController.network(widget.url);
    await _videoPlayerController.initialize();

    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
      looping: false,
      hideControlsTimer: const Duration(seconds: 1),
      additionalOptions: (context) =>
      <OptionItem>[
        OptionItem(
            onTap: (){},
            iconData: Icons.add_box,
            title: 'add'
        )
      ],
    );

  return;
  }

  bool get isInit =>
      (_chewieController != null ?
      _chewieController!.videoPlayerController.value.isInitialized
          :
      false);


  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController?.dispose();
    print('dispose');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: (900/16).w,
      child: FutureBuilder(
        future: init(),
        builder: (context,snapShot){
          if(isInit)
            return Chewie(
                controller: _chewieController!,
            );

            return Center(
              child: (snapShot.hasError) ?
              Text('Error'):
              CircularProgressIndicator()
              ,
            );
        },
      ),
    );
  }
}
