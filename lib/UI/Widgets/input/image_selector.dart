import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

class ImageSelector extends StatefulWidget {

  final function;
  String? _path;

  ImageSelector(this.function);

  @override
  State<ImageSelector> createState() => _ImageSelectorState();
}

class _ImageSelectorState extends State<ImageSelector> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        var picker = ImagePicker();
        var xfile = await picker.pickImage(source: ImageSource.gallery);

        setState(() {
          if(xfile != null) {
            widget._path = xfile.path;
          }
          else
            widget._path = null;
          widget.function(widget._path);
        });


      },
      child: Container(
        width: double.infinity,
        height: 25.h,
        margin: EdgeInsets.all(2.5.w),
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.25),
          borderRadius: BorderRadius.circular(2.5.w),
          image: widget._path == null ? null :
              DecorationImage(
                  image: FileImage(File(widget._path!)),
                  fit: BoxFit.cover
              ),
        ),
        alignment: Alignment.center,
        child: widget._path == null ?
        Icon(Icons.add_a_photo_outlined,size: 40.sp,):
        null
        ,
      ),
    );
  }
}
