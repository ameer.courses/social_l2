import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';


class PageImage extends StatelessWidget {
  final String path;
  PageImage(this.path);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50.w,
      height: 50.w,
      margin: EdgeInsets.all(2.5.w),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(path),
              fit: BoxFit.cover
          ),
          borderRadius: BorderRadius.circular(2.5.w)
      ),

    );
  }
}
