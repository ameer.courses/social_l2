import 'package:flutter/material.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:social/UI/Widgets/videoWidget.dart';

import '../../../Logic/API/Models/postModel.dart';

class PostView extends StatelessWidget {


  final PostModel model;

  PostView(this.model);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.5.w),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: _user,
              title: Text(model.userName),
              subtitle: Text(model.createdAt.toString()),
            ),
            Divider(
              height: 0,
            ),
            Padding(
              padding: EdgeInsets.all(1.w),
              child: Text(model.content),
            ),
            Visibility(
                visible: model.file != null && model.type == 'image',
                child: Center(
                  child: FancyShimmerImage(
                    imageUrl: model.file!,
                    errorWidget: Center(
                      child: Icon(Icons.image_not_supported_outlined,size: 50.sp,),
                    ),
                  ),
                ),
            ),
            Visibility(
              visible: model.file != null && model.type == 'video',
              child: Container(
                margin: EdgeInsets.all(2.5.w),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.25),
                  borderRadius: BorderRadius.circular(2.5.w)
                ),
                width: double.infinity,
                height: (900/16).w,
                alignment: Alignment.center,
                child: InkWell(
                  child: Icon(Icons.play_circle,size: 45.sp,),
                  onTap:()=>
                      Get.dialog(
                      Dialog(
                        insetPadding: EdgeInsets.zero,
                        child: VideoWidget(model.file!),
                      )
                  ),
                )
              ),
            ),
            Divider(),
            Reactions(model)
          ],
        ),
      ),
    );
  }

  Widget get _user{
    if(model.userImage != null)
      return CircleAvatar(
        foregroundImage: NetworkImage(model.userImage!),
      );
    return CircleAvatar(
      child: Icon(Icons.person),
    );
  }
}

class Reactions extends StatelessWidget {

  final PostModel model;


  Reactions(this.model);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Row(
          children: [
            Text(model.likes.toString()),
            SizedBox(width: 2.w,),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.thumb_up_alt_outlined)
            ),
          ],
        ),
        Row(
          children: [
            Text(model.dislikes.toString()),
            SizedBox(width: 2.w,),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.thumb_down_alt_outlined)
            ),
          ],
        ),
        Row(
          children: [
            Text(model.comments.toString()),
            SizedBox(width: 2.w,),
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.comment)
            ),
          ],
        ),
      ],
    );
  }
}
