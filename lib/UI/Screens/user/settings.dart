import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social/Logic/STM/Services/storageService.dart';

class SettingsContent extends StatelessWidget {

  final storage = Get.find<StorageService>();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        SwitchListTile(
            value: storage.isDark,
            onChanged: (value){

            }
        )
      ],
    );
  }
}
