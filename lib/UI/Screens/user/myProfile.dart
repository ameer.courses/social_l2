import 'package:flutter/material.dart';
import 'package:social/Logic/API/Controllers/PostController.dart';
import 'package:social/UI/Widgets/profile/profileHeader.dart';

import '../../../Logic/API/Models/postModel.dart';
import '../../Widgets/post/postView.dart';

class MyProfile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ProfileHeader(),
        FutureBuilder<List<PostModel>>(
          future: PostController.getMyProfile(),
          builder: (context, snapshot) {
            if(snapshot.hasData) {
                return ListView.builder(
                    physics: PageScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) =>
                        PostView(snapshot.data![i])
                );
              }
            return Center(
              child: snapshot.hasError ?
              Text('Error : ${snapshot.error}'):
              CircularProgressIndicator()
              ,
            );
            }
        )
      ],
    );
  }
}
