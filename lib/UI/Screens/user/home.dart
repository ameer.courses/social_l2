import 'package:circle_nav_bar/circle_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/STM/Controllers/homeController.dart';
import 'package:social/UI/Screens/user/settings.dart';
import 'package:social/UI/Widgets/post/postView.dart';

import 'myProfile.dart';

class HomeScreen extends StatelessWidget {

  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        title: Text('Home'),
      ),

      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: homeController.pageController,
        children: [
          MyProfile(),
          HomeContent(),
          SettingsContent()
        ],
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        child: GetX<HomeController>(
            builder: (controller){
              return CircleNavBar(
                activeIcons: const [
                  Icon(Icons.person),
                  Icon(Icons.home),
                  Icon(Icons.settings)
                ],
                inactiveIcons:const [
                  Icon(Icons.person_outline),
                  Icon(Icons.home_outlined),
                  Icon(Icons.settings_outlined)
                ],
                color: Theme.of(context).primaryColor,
                height: 8.h,
                activeIndex: controller.pageInx.value,
                circleWidth: 8.h,
                onTab: (val){
                  controller.toPage(val);
                },
                iconDurationMillSec: 400,
              );
            },
        ),
      ),


    );
  }
}

class HomeContent extends StatelessWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return ListView.builder(
    //     itemCount: 10,
    //     itemBuilder: (context,i) => PostView(
    //       hasImage: i.isEven,
    //       hasPostImage: i < 3,
    //     )
    // );
    return Container();
  }
}
