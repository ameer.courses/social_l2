import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/API/Controllers/PostController.dart';
import 'package:social/Logic/Validators/post.dart';
import 'package:social/UI/Widgets/input/myTextFiels.dart';

import '../../utils/routes.dart';

class CreatePost extends StatelessWidget {

  final TextEditingController contentController = TextEditingController();
  String? _path;
  String? _type;// video , image

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create a post'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: CustomField(
                maxLines: 7,
                controller: contentController,
                hint: 'Create a new post',
                validator: PostValidator.content,
              ),
            ),
            StatefulBuilder(
              builder: (context, setState) {
                return ListTile(
                  leading: Icon(Icons.file_copy_outlined),
                  title: Text('add image or video'),
                  subtitle: _path != null ? Text(_path!) : null,
                  trailing: _path == null ?Icon(Icons.add):
                    IconButton(
                        onPressed: ()=>setState((){
                          _path = _type = null;
                        }),
                        icon: Icon(Icons.remove_circle_outline)
                    ),
                  onTap: ()=>showModalBottomSheet(
                      context: context,
                      builder: (context)=>Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ListTile(
                            leading: Icon(Icons.slow_motion_video),
                            title: Text('video'),
                            onTap: ()async{
                              var imagePicker = ImagePicker();
                              var xfile = await imagePicker.pickVideo(source: ImageSource.gallery);
                              setState((){
                                if(xfile != null) {
                                    _path = xfile.path;
                                    _type = 'video';
                                  }
                                });
                            },
                          ),
                          ListTile(
                            leading: Icon(Icons.camera),
                            title: Text('image'),
                            onTap: ()async{
                              var imagePicker = ImagePicker();
                              var xfile = await imagePicker.pickImage(source: ImageSource.gallery);
                              setState((){
                                if(xfile != null) {
                                  _path = xfile.path;
                                  _type = 'image';
                                }
                              });
                            },
                          ),
                        ],
                      )
                  ),
                );
              }
            ),
            SizedBox(
              height: 10.w,
            ),
            ElevatedButton(
                onPressed: ()=>create(context),
                child: Text('Create')
            ),
          ],
        ),
      ),
    );
  }

  void create(context) async{
    if(! _formKey.currentState!.validate()){
      Get.snackbar('error', 'post content is wrong');
      return;
    }
    showDialog(
        context: context,
        builder: (context)=>
            WillPopScope(
                child: Center(child: CircularProgressIndicator()),
                onWillPop: ()async{
                  return true;
                }
            )
    );
    var code = await PostController.createPost(
        content: contentController.text,
        file: _path,
        type: _type
    );
    Get.back();
    if(code == 200){
      Get.offAllNamed(Routes.home);
    }
    else{
      Get.snackbar('error', 'failed to create post $code');
    }

  }
}
