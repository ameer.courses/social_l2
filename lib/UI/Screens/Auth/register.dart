import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/API/Controllers/AuthController.dart';
import 'package:social/Logic/Validators/auth.dart';
import 'package:social/UI/Widgets/input/myTextFiels.dart';
import 'package:social/UI/Widgets/pagesImages.dart';
import 'package:social/utils/routes.dart';

import '../../../utils/images.dart';
import '../../Widgets/input/image_selector.dart';

class RegisterScreen extends StatelessWidget {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _passController = TextEditingController();
  final TextEditingController _cpassController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  String? _path;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('register'),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: SvgPicture.asset(Images.registerBackground,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  PageImage(Images.registerImage),
                  CustomField(
                    hint: 'name',
                    controller: _nameController,
                    keyboardType: TextInputType.name,
                    validator: AuthValidator.name,
                    icon: Icon(Icons.person_outline),
                  ),
                  CustomField(
                    hint: 'email',
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: AuthValidator.email,
                    icon: Icon(Icons.email_outlined),
                  ),
                  CustomField(
                    keyboardType: TextInputType.phone,
                    hint: 'phone',
                    controller: _phoneController,
                    validator: AuthValidator.phone,
                    icon: Icon(Icons.phone_outlined),
                  ),
                  CustomField(
                    controller: _passController,
                    hint: 'password',
                    validator: AuthValidator.password,
                    isPass: true,
                    icon: Icon(Icons.lock_outline),
                  ),
                  CustomField(
                    controller: _cpassController,
                    hint: 'confirm password',
                    validator: (value){
                      var error = AuthValidator.password(value);
                      if(error != null)
                        return error;

                      if(_cpassController.value != _passController.value)
                        return 'error confirm password is wrong';

                    },
                    isPass: true,
                    icon: Icon(Icons.lock_outline),
                  ),
                  ImageSelector((String value){
                    _path = value;
                  }),

                  ElevatedButton(
                      onPressed: () async{
                        if(!_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text('data error')));
                        }
                        else{
                          Get.dialog(
                              WillPopScope(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  onWillPop: () async {
                                    return true;
                                  }
                              )
                          );

                          var token = await AuthController.register(
                              name: _nameController.text,
                              email: _emailController.text,
                              phone: _phoneController.text,
                              password: _passController.text,
                            image: _path
                          );
                          Get.back();
                          if(token != null)
                            Get.offAllNamed(Routes.home);
                          else
                            Get.snackbar('register faild', 'Error in data');
                        }
                      },
                      child: Text('register'),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  InkWell(
                    child: Text('login if you have an account',
                    style: TextStyle(
                      fontSize: 12.sp
                    ),
                    ),
                    onTap: ()=>Get.offNamed(Routes.login),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).viewInsets.bottom + 5.h,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
