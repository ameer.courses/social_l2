
class PostValidator{


  static String? content(String? value){
    if( value == null || value.isEmpty)
      return 'write any content to post it.';
    else if( value.length > 2000)
      return 'the text can not be more than 2000 character.';
  }

}