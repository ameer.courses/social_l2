import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
//import 'package:get_storage/get_storage.dart';


class StorageService extends GetxService{

  late final GetStorage _storage ;

  @override
  void onInit() {
    _storage = GetStorage();
  }

  bool get isAuth {
    return _storage.hasData('token');
  }

  String get token => _storage.read('token') ?? '';

  Future<void> login(token) async{
    await _storage.write('token', token);
  }

  Future<void> logout() async{
    await _storage.remove('token');
  }


  Future<void> setTheme(bool val) async{
    _storage.write('theme' , val);
  }

  bool get isDark => _storage.read('theme') ?? true;

  Future<void> setLang(bool val) async{
    _storage.write('lang' , val);
  }

  bool get isEn => _storage.read('lang') ?? true;

}