import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social/utils/routes.dart';

import '../Services/storageService.dart';

class AuthMiddleware extends GetMiddleware{

  @override
  RouteSettings? redirect(String? route) {
    var storage = Get.find<StorageService>();
    if(storage.isAuth)
     return RouteSettings(
      name: Routes.home
     );



  }
}

