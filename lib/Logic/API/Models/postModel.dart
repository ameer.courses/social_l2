
class PostModel {

  static String url = '';

  PostModel({
    required this.id,
    required this.userId,
    required this.content,
    required this.likes,
    required this.dislikes,
    required this.comments,
    required this.createdAt,
    required this.updatedAt,
    required this.userName,
    required this.userImage,
    required this.file,
    required this.type,
  });

  final int id;
  final int userId;
  final String content;
  final int likes;
  final int dislikes;
  final int comments;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String userName;
  final String? userImage;
  final String? file;
  final String? type;

  factory PostModel.fromJson(Map<String, dynamic> json) => PostModel(
    id: json["id"] ?? 0,
    userId: json["user_id"] ?? 0,
    content: json["content"] ?? '',
    likes: json["likes"] ?? -1,
    dislikes: json["dislikes"] ?? -1,
    comments: json["comments"] ?? -1,
    createdAt: DateTime.tryParse(json["created_at"]) ?? DateTime.now(),
    updatedAt: DateTime.tryParse(json["updated_at"]) ?? DateTime.now(),
    userName: json["user_name"] ?? '',
    userImage: json["user_image"] != null ? url + json["user_image"] : null,
    file: json["file"] != null ? url + json["file"] : null,
    type: json["type"] ?? '',
  );


}
