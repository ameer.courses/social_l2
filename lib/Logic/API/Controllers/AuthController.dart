import 'dart:async';
import 'dart:convert';
import 'package:get/get.dart';
import 'package:social/Logic/STM/Services/storageService.dart';

import '../../../utils/api.dart';
import 'package:http/http.dart' as http;


class AuthController{

  static Future<String?> login(String email,String password) async {
    var response = await http.post(
      Uri.parse(Api.login),
      body: jsonEncode({
        'email':email,
        'password':password
      }),
      headers: {
        'Content-Type':'application/json',
        'Accept':'application/json',
      }
    );
    Map<String,dynamic> json = jsonDecode(response.body);
    if(response.statusCode == 200) { // json['token'] != null
      var storage = Get.find<StorageService>();
      await storage.login(json['token']);
      return json['token'];
    }
  }

  static Future<String?> register({
    required String name,
    required String email,
    required String phone,
    required String password,
    String? image
}) async{
    var request = http.MultipartRequest('POST',
        Uri.parse(Api.register));
    request.fields.addAll({
      'name': name,
      'email': email,
      'password': password,
      'phone': phone
    });
    request.headers.addAll({
      'Accept':'application/json'
    });
    if(image != null)
    request.files.add(
        await http.MultipartFile.fromPath(
            'image',
            image)
    );

    var response = await request.send();

    // no need
    if(response.statusCode == 200){
      Map<String,dynamic> json = jsonDecode(
          await response.stream.bytesToString()
      );
      // if json['token'] != null
      var storage = Get.find<StorageService>();
      await storage.login(json['token']);
      return json['token'];
    }


  }

}

