import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:social/Logic/API/Models/postModel.dart';
import 'package:social/Logic/STM/Services/storageService.dart';

import '../../../utils/api.dart';


class PostController{

  static Future<int> createPost({
  required String content,
    String? file,
    String? type
}) async{
    var request = http.MultipartRequest('POST',
        Uri.parse(Api.createPost));
    var storage = Get.find<StorageService>();
    request.headers.addAll({
      'Accept' :'application/json',
      'Authorization': 'Bearer ${storage.token}'
    });

    request.fields.addAll({
      'content':content
    });

    if(type != null && file != null) {
      request.fields.addAll({
        'type': type
      });
      request.files.add(
          await http.MultipartFile.fromPath('file', file));
    }



    var response = await request.send();

    return response.statusCode;

  }


  static Future<List<PostModel>> getMyProfile() async{
    var storage = Get.find<StorageService>();
    var response = await http.get(
      Uri.parse(Api.myProfiler),
      headers: {
        'Accept':'application/json',
        'Authorization': 'Bearer ${storage.token}'
      }
    );
    if(response.statusCode != 200)
      return [];
    Map<String,dynamic> json = jsonDecode(response.body);
    log(json['url'].toString());
    List<PostModel> posts = [];
    for(var p in json['posts'])
      posts.add(
        PostModel.fromJson(p)
      );

    print(posts);

    return posts;
  }


}
